﻿declare module GeoJSON {

    export interface CRSProperties {
        name?: string;
        href?: string;
        type?: string;
    }

    export interface CRS {
        type: string;
        properties: CRSProperties;
    }

    export interface GeoJSON {
        type: string;
        crs?: CRS;
        bbox?: number[];
    }

    export interface Geometry extends GeoJSON {
    }

    export interface Point extends Geometry {
        coordinates: number[];
    }

    export interface MultiPoint extends Geometry {
        coordinates: number[][];
    }

    export interface LineString extends Geometry {
        coordinates: number[][];
    }

    export interface MultiLineString extends Geometry {
        coordinates: number[][][];
    }

    export interface Polygon extends Geometry {
        coordinates: number[][][];
    }

    export interface MultiPolygon extends Geometry {
        coordinates: number[][][][];
    }

    export interface GeometryCollection extends GeoJSON {
        geometries: Geometry[];
    }

    export interface Feature extends GeoJSON {
        geometry: Geometry;
        id?: string;
        properties?: any;
    }

    export interface FeatureCollection extends GeoJSON {
        features: Feature[];
    }


} 