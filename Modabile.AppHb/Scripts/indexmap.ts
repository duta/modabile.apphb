﻿/// <reference path="typings/jquery/jquery.d.ts"/>
/// <reference path="typings/openlayers/openlayers.d.ts"/>
/// <reference path="typings/geojson/geojson.d.ts"/>

interface SharedVar {
    zoom: number;
    layerMap: OpenLayers.Layer.Vector;
    layerShip: OpenLayers.Layer.Vector;
    layerZona: OpenLayers.Layer.Vector;
    layerCCTV: OpenLayers.Layer.Vector;
    layerChosenShip: OpenLayers.Layer.Vector;
    layerNavStatus: OpenLayers.Layer.Vector;
    featuresShipOnDisplay: OpenLayers.Format.GeoJSON;
}