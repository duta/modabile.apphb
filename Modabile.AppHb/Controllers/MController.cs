﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Modabile.AppHb.Controllers
{
    public class MController : Controller
    {
        public ViewResult Index()
        {
            ViewBag.Title = "Modabile";
            int hour = DateTime.Now.Hour;
            ViewBag.Greeting = hour < 12 ? "Good Morning" : "Good Afternoon";
            return View();
        }

    }
}
